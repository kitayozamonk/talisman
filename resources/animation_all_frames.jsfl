var dom = fl.getDocumentDOM();
tl = dom.getTimeline();
currLayer = tl.currentLayer;
//capture keyframes in layer 2 of timeline
fl.getDocumentDOM().getTimeline().convertToKeyframes();
var frameArray = fl.getDocumentDOM().getTimeline().layers[currLayer].frames;
var item = fl.getDocumentDOM().getTimeline().layers[currLayer].frames.elements;
var frameArrayLen = frameArray.length;

data = '';
for (var j=0; j < frameArrayLen; j++) {

    if (j == frameArray[j].startFrame) {
        //fl.trace("Keyframe at: " + j);

        currFrame=j;
        var item = fl.getDocumentDOM().getTimeline().layers[currLayer].frames[currFrame].elements[0];
        if (item == undefined) {
            continue;
        }

        ix=m_round(item.x)//item.x;
           iy=m_round(item.y)//item.y;
              ir=m_round(item.rotation)//item.rotation;

                 ixs=m_round(item.scaleX);
        iys=m_round(item.scaleY);

        iw=m_round(item.width);
        ih=m_round(item.height);

        ia=item.colorAlphaPercent;

        if (data != '') data += ',';
        data+='['+ix+','+iy+']';
        //data+='['+ix+','+iy+','+ir+','+ixs+','+iys+','+iw+','+ih+','+ia+','+currFrame+']';
    }
}
fl.trace(data);
fl.undo;

function m_round(num) {
    var res=Math.round(num*100)/100;
    return (res);
}