﻿using UnityEngine;
using System.Collections;

public class Masking : MonoBehaviour {
	public MeshFilter background;
	// Use this for initialization
	//void Awake ();
	void Start () {
		GenerateTexture ();	
		//background.SetTexture("_BumpMap", mask);
	}
	
	// Update is called once per frame
	void Update () {
			
	}
	private int texWidth = 512;
	private int texHeight = 512;
	
	//MASK SETTINGS
	private float maskThreshold = 2.0f;
	
	//REFERENCES
	Texture2D mask;
	
	public void GenerateTexture(){
		
		mask = new Texture2D(texWidth, texHeight, TextureFormat.RGBA32, true);
		Vector2 maskCenter = new Vector2(texWidth * 0.5f, texHeight * 0.5f);
		
		for(int y = 0; y < texHeight; ++y){
			for(int x = 0; x < texWidth; ++x){
				
				float distFromCenter = Vector2.Distance(maskCenter, new Vector2(x, y));
				float maskPixel = (0.5f - (distFromCenter / texWidth)) * maskThreshold;
				mask.SetPixel(x, y, new Color(maskPixel, maskPixel, maskPixel, 1.0f));
			}
		}
		mask.Apply();
	}
}
