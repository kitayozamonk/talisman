﻿using UnityEngine;
using System.Collections;

public class GameMaster : NIApplication {
	public Camera cameraPipe;
	public Camera cameraBG;
	public UIRoot root;
	public GUITexture background;
	public GameObject introScreen;
	public GameObject mainScreen;
	public GameObject shopScreen;
	public GameObject talismanScreen;
	public GameObject tipsScreen;
	public GameObject tutorialScreen;
	public float x;
	public float y;
	public float w;
	public float h;
	private float resizeK;
	public float lastScreenWidth = 0f;

	// Use this for initialization
	protected void Start () {
		base.Start ();
		Rect r = cameraPipe.rect;
		//cameraPipe.rect = viewport;
		background.pixelInset = new Rect(viewport.x*Screen.width,viewport.y*Screen.height,-2*viewport.x*Screen.width,-2*viewport.y*Screen.height);
		print("CameraPipe displays from " + r.xMin + " to " + r.xMax + " pixel");
		r = cameraBG.rect;
		lastScreenWidth = Screen.width;
		resizeK = 1 / root.transform.localScale.x;
		//cameraBG.rect = viewport;
		print("CameraPipe displays from " + r.xMin + " to " + r.xMax + " pixel");
//		NGUITools.SetActive (introScreen,true);
//		NGUITools.SetActive (mainScreen,false);
//		NGUITools.SetActive (shopScreen,false);
//		NGUITools.SetActive (talismanScreen,false);
//		NGUITools.SetActive (tipsScreen,false);
//		NGUITools.SetActive (tutorialScreen,false);
	} 

	protected void Update (){
		base.Update ();
		if( lastScreenWidth != Screen.width ){
			lastScreenWidth = Screen.width;
			resizeK = 1 / root.transform.localScale.x;
			print ("K=" + resizeK +" W="+Screen.width+" H="+Screen.height+" NW="+cellX*cellSize+" NH="+cellY*cellSize);

		}
		//cameraPipe.rect = viewport;
		background.pixelInset = new Rect(viewport.x*Screen.width,viewport.y*Screen.height,-2*viewport.x*Screen.width,-2*viewport.y*Screen.height);
		//cameraBG.rect = viewport;
	}


	public void sendLog(){
		Rect r = cameraPipe.rect;
		print("CameraPipe displays from " + r.xMin + " to " + r.xMax + " pixel");
		r = cameraBG.rect;
		print("CameraBG displays from " + r.xMin + " to " + r.xMax + " pixel");
//		Debug.Log("Screen height = "+Screen.height);
//		Debug.Log("Screen width = "+Screen.width);
//		Debug.Log("offsetX = "+offsetX);
//		Debug.Log("offsetY = "+offsetY);
	}

	public Camera CameraPipe {
		get {
			return cameraPipe;
		}
	}

	public float ResizeK {
		get {
			return resizeK;
		}
	}
}
