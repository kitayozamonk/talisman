using UnityEngine;
using System.Collections;

public class TipsScreen : MonoBehaviour {
	public GameObject mainScreen;
	public GameObject tutorialScreen;
	public GameObject dateEnterScreen;

	// Use this for initialization	
	void Start () {
	
	}
	void Awake(){
		//NGUITools.SetActive(introScreen,false);		
	}
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable() {
		//NGUITools.SetActive(introScreen,false);
	}

	public void proceed() {
		PlayerPrefs.SetInt("firsttime",1);
		NGUITools.SetActive (this.gameObject, false);
		if (PlayerPrefs.GetInt("firsttime",1) == 1) {
			PlayerPrefs.SetInt("roundsleft",3);
						NGUITools.SetActive (dateEnterScreen, true);
						
				} else {
						NGUITools.SetActive (mainScreen, true);
				}

	}
	public void tutorial() {
		NGUITools.SetActive (tutorialScreen, true);

		NGUITools.SetActive (this.gameObject, false);
	}

}
