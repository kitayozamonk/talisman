﻿using UnityEngine;
using System.Collections;

public class ShopScreen : MonoBehaviour {

	public GameObject shopScreen;
	public GameObject mainScreen;	
	
	public void proceed() {
		NGUITools.SetActive (shopScreen,true);
		NGUITools.SetActive (mainScreen,false);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
