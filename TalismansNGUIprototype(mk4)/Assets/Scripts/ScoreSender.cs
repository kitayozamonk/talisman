﻿using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreSender : MonoBehaviour {
	
	string url = "http://okmarket.ru/customers/game2/sendtosocial.php"; // Your URL copy here
	
	bool sema = false;

	string UrlEncode(string instring)
	{
		StringReader strRdr = new StringReader(instring);
		StringWriter strWtr = new StringWriter();
		int charValue = strRdr.Read();
		while (charValue != -1)
		{
			if (((charValue >= 48) && (charValue <= 57)) // 0-9
			    || ((charValue >= 65)  && (charValue <= 90)) // A-Z
			    || ((charValue >= 97)  && (charValue <= 122))) // a-z
			{
				strWtr.Write((char) charValue);
			}
			else if (charValue == 32) // Space
			{
				strWtr.Write("+");
			}
			else
			{
				strWtr.Write("%{0:x2}", charValue);
			}
			charValue = strRdr.Read();
		}
		return strWtr.ToString();
	}
	
	void SendLog(string sessionID,int score) {
		//?sessiionID=12eb456b &score=331
		if (sema) return;
		sema = true;
		string t_url = url + "?sessiionID=" + UrlEncode(sessionID) + "&score=" + score;
		WWW www = new WWW(t_url);
		StartCoroutine(WaitForRequest(www));
	}
	
	IEnumerator WaitForRequest(WWW www)
	{
		yield return www;
		// check for errors
		if (www.error == null) {
			//OK
		} else {
			//Error
		}
		sema = false;
	}
}