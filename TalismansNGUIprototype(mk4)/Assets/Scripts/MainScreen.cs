using UnityEngine;
using System.Collections;

public class MainScreen : MonoBehaviour {

	public GameObject mainScreen;
	public GameObject shopScreen;
	public GameObject tipsScreen;
	public GameObject tutorialScreen;
	public GameObject loveScreen;
	public GameObject healthScreen;
	public GameObject protectionScreen;
	public GameObject luckScreen;

	
	public void proceed() {
		NGUITools.SetActive (mainScreen,true);
		NGUITools.SetActive (shopScreen,false);
		NGUITools.SetActive (tipsScreen,false);
		NGUITools.SetActive (tutorialScreen,false);
		NGUITools.SetActive (loveScreen,false);
		NGUITools.SetActive (healthScreen,false);
		NGUITools.SetActive (protectionScreen,false);
		NGUITools.SetActive (luckScreen,false);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
