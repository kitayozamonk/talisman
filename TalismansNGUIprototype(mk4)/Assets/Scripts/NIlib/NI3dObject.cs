﻿using UnityEngine;
using System.Collections;

public class NI3dObject : NIDisplayObject {
	private Camera cam3D;
	private float proportionZ;

	// Use this for initialization
	new void Start () {
		GameObject[] cameras = GameObject.FindGameObjectsWithTag("3d");
		foreach (GameObject cams in cameras){
			cam3D =cams.GetComponent<Camera>() as Camera;
		}
		proportionZ = Mathf.Sqrt(this.transform.localScale.x*this.transform.localScale.y)/this.transform.localScale.z;	
	

	}
	
	// Update is called once per frame
	new void Update () {
		gameObject.transform.localPosition = new Vector3(GameMaster.CellSize*x+GameMaster.OffsetX,
		                                                 (-1*GameMaster.CellSize*y+GameMaster.OffsetY),
		                                                 (GameMaster.getZk()*Mathf.Sqrt(height*width)-GameMaster.getZb()));//Mathf.Sqrt(GameMaster.CellSize*width*GameMaster.CellSize*height)/proportionZ/2);
		gameObject.transform.localScale = new Vector3(width,
		                                              height,
		                                              Mathf.Sqrt(width*height)/proportionZ);
	
	}
}
