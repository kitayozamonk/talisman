﻿using UnityEngine;
using System.Collections;

public class NIWidget : NIDisplayObject {
	private UIWidget widget;

	// Use this for initialization
	new void Start () {
		widget = (UIWidget)this.GetComponent ("UISprite");
		if(!widget)
			widget = (UIWidget)this.GetComponent ("UILabel");

	
	}
	
	// Update is called once per frame
	new void Update () {
		if (widget) {
						widget.SetRect (x * GameMaster.CellSize + GameMaster.OffsetX, 
		                -y * GameMaster.CellSize + GameMaster.OffsetY - (GameMaster.CellSize * height), 
		                (GameMaster.CellSize * width), (GameMaster.CellSize * height));	
			NGUITools.UpdateWidgetCollider(gameObject);
				}

	}
}
