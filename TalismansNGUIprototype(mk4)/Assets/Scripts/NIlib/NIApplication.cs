using UnityEngine;
using System.Collections;

public class NIApplication : MonoBehaviour {
	protected static float cellSize;
	protected static float offsetX;
	protected static float offsetY;
	protected static Rect viewport;
	protected static Texture2D t ;
	public static int cellY=60;
	public static int cellX=40;
	public bool drawNet = false;

	void Awake (){
		recalculateSizes ();
	}
	// Use this for initialization
	protected void Start () {
		recalculateSizes ();
	}
	
	// Update is called once per frame
	protected void Update () {
		recalculateSizes ();
	}
	void recalculateSizes(){

		if ((float)Screen.width/cellX > (float)Screen.height/cellY) {
			cellSize = (float)Screen.height / cellY;
			offsetX = -1 * cellX * cellSize / 2;
			offsetY = cellY * cellSize / 2;
		} else {
			cellSize = (float)Screen.width / cellX;
			offsetX = -1 * cellX * cellSize / 2;
			offsetY = cellY * cellSize / 2;
		}
		viewport = new Rect (((1-cellSize*cellX/Screen.width)/2),
		                     ((1-cellSize*cellY/Screen.height)/2),
		                     cellSize*cellX/Screen.width,
		                     cellSize*cellY/Screen.height);
//		viewport = new Rect (((1-cellSize*cellX/Screen.width)/2),
//		                     0.5f, 0.25f, 0.75f);
	}
	void OnGUI(){
		if (drawNet) {
						if (!t) {
								t = new Texture2D (1, 1);
						}
						for (int i=0; i<cellX+1; i++) {	
								GUI.DrawTexture (new Rect (cellSize * i + offsetX + Screen.width / 2, Screen.height / 2 - offsetY, 1, cellSize * cellY), t);
						}
						for (int i=0; i<cellY+1; i++) {
								GUI.DrawTexture (new Rect (offsetX + Screen.width / 2, cellSize * i + Screen.height / 2 - offsetY, cellSize * cellX, 1), t);
								//Drawing.DrawLine(new Vector2(0,cellHeight*1),new Vector2(Screen.width,cellHeight*1));
						}
				}
	}

	public static float getZk(){
		return cellSize / 2;
	}
	public static float getZb(){
		return ((float)Screen.width/cellX > (float)Screen.height/cellY)?(Screen.height/3.57f):(Screen.width/3.57f);
	}

	public static float CellSize {
		get {
			return cellSize;
		}
	}	

	public static int CellY {
		get {
			return cellY;
		}
	}

	public static int CellX {
		get {
			return cellX;
		}
	}

	public static float OffsetX {
		get {
			return offsetX;
		}
	}

	public static float OffsetY {
		get {
			return offsetY;
		}
	}
}
