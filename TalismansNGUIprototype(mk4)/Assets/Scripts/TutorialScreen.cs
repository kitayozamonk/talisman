﻿using UnityEngine;
using System.Collections;

public class TutorialScreen : MonoBehaviour {

	public GameObject mainScreen;
	public GameObject tutorialScreen;
	public GameObject tipsScreen;


	public void openTutorial() {
		NGUITools.SetActive (tipsScreen,false);
		NGUITools.SetActive (tutorialScreen,true);
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
