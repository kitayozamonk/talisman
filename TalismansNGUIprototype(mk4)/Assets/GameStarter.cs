﻿using UnityEngine;
using System.Collections;

public class GameStarter : MonoBehaviour {

	public GameObject introScreen;
	public UIPanel mainScreen;
	public UIPanel shopScreen;
	public UIPanel talismanScreen;
	public UIPanel tipsScreen;
	public UIPanel tutorialScreen;

	// Use this for initialization
	void Start () {
		NGUITools.SetActive (introScreen,true);
//		introScreen.enabled=true;
//		mainScreen.enabled=false;
//		shopScreen.enabled=false;
//		talismanScreen.enabled=false;
//		tipsScreen.enabled=false;
//		tutorialScreen.enabled=false;	
	} 

	void Awake(){

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
